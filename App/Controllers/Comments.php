<?php

namespace App\Controllers;

use App\Models\Comment;

use Neomerx\JsonApi\Document\Link;
use Neomerx\JsonApi\Encoder\Encoder;
use Neomerx\JsonApi\Encoder\EncoderOptions;
use Neomerx\JsonApi\Encoder\Parameters\EncodingParameters;

use JSONAPI\Schemas\CommentSchema;

/**
 * Comments controller
 *
 * PHP version 7.0
 */
class Comments extends \Core\Controller
{
    private $_data = null;

    private function convertDataToInstances($data) {
        return array_map(function($comment) {
            return \JSONAPI\Models\Comment::instance($comment['id'], $comment['message'], $comment['dt_created'], $comment['dt_updated']);
        }, $data);
    }

    public function indexGetAction()
    {
        $comments = Comment::getAll();

        $this->_data = $this->convertDataToInstances($comments);
    }

    public function indexPostAction()
    {
        if (!isset($_POST['message'])) {
            throw new \Exception('Message required.', 422);
        }

        $id = Comment::add($_POST['message']);

        $this->route_params['id'] = $id;

        $this->rudGetAction();
    }

    public function rudGetAction() {
        $comment = Comment::getById($this->route_params['id']);

        $this->_data = $this->convertDataToInstances($comment)[0];
    }

    public function rudDeleteAction() {
        Comment::remove($this->route_params['id']);

        $this->indexGetAction();
    }

    public function rudPutAction() {
        parse_str(file_get_contents("php://input"),$post_vars);

        if (!isset($post_vars['message'])) {
            throw new \Exception('Message required.', 422);
        }

        $id = $this->route_params['id'];

        Comment::update($id, $post_vars['message']);

        $this->rudGetAction();
    }

    public function after() {
        $encoder = Encoder::instance([
            \JSONAPI\Models\Comment::class => CommentSchema::class,
        ], new EncoderOptions(JSON_PRETTY_PRINT, 'http://simplecrud'));

        echo $encoder->encodeData($this->_data);
    }
}
