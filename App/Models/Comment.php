<?php

namespace App\Models;

use PDO;

/**
 * Example comments model
 *
 * PHP version 7.0
 */
class Comment extends \Core\Model
{

    /**
     * Get all the comments as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM comments');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getById($id)
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM comments WHERE id = '.$db->quote($id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function add($message)
    {
        $db = static::getDB();
        $db->query('INSERT INTO comments (message) VALUES ('.$db->quote($message).')');
        return $db->lastInsertId();
    }

    public static function remove($id)
    {
        $db = static::getDB();
        return $db->query('DELETE FROM comments WHERE id = '.$db->quote($id));
    }

    public static function update($id, $message)
    {
        $db = static::getDB();
        return $db->query('UPDATE comments set message = '.$db->quote($message).' WHERE id = '.$db->quote($id));
    }
}
