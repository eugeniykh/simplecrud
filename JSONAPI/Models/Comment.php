<?php namespace JSONAPI\Models;

/**
 * @package Neomerx\Samples\JsonApi
 *
 * @property int    commentId
 * @property string message
 * @property timestamp $createdAt
 * @property timestamp $updatedAt
 */
class Comment extends \stdClass
{
    /**
     * @param string $commentId
     * @param string $message
     * @param timestamp $createdAt
     * @param timestamp $updatedAt
     *
     * @return Comment
     */
    public static function instance($commentId, $message, $createdAt, $updatedAt)
    {
        $comment = new self();

        $comment->commentId  = $commentId;
        $comment->message = $message;
        $comment->createdAt = $createdAt;
        $comment->updatedAt = $updatedAt;

        return $comment;
    }
}