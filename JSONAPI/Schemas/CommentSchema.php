<?php namespace JSONAPI\Schemas;

use Neomerx\JsonApi\Schema\BaseSchema;
use Neomerx\Samples\JsonApi\Models\Author;

/**
 * @package Neomerx\Samples\JsonApi
 */
class CommentSchema extends BaseSchema
{
    /**
     * @inheritdoc
     */
    protected $resourceType = 'comments';

    /**
     * @inheritdoc
     */
    public function getId($comment): ?string
    {
        /** @var Author $author */
        return $comment->commentId;
    }

    /**
     * @inheritdoc
     */
    public function getAttributes($comment, array $fieldKeysFilter = null): ? array
    {
        /** @var Author $author */
        return [
            'message' => $comment->message,
            'createdAt' => $comment->createdAt,
            'updatedAt' => $comment->updatedAt
        ];
    }
}