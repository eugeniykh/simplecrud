# Simple CRUD

1. run composer install
2. Use .env file to configure connection to DB with next configuration:

DB_HOST=

DB_NAME=

DB_USER=

DB_PASSWORD=

After deploying, please, create database:

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `dt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

Now...
 
 using url comments/ POST Param (x-www-form-urlencoded: message) you can create comment with requested message.
 
 using url comments/{id} GET where id - id of your comment you want to look
 
 using url comments/{id} DELETE where id - id of your comment you want to delete
 
 using url comments/{id} PUT (x-www-form-urlencoded: message) where id - id of your comment you want to update with the requested message